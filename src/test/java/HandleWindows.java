import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class HandleWindows {
    WebDriver driver;
    private static Properties properties;

    @BeforeClass
    public void setUp(ITestContext context) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        context.setAttribute("WebDriver", driver);
    }

    @Test
    public void handleWindow() throws IOException {
        PropertyReader pr= new PropertyReader();
        properties = pr.propertyReader();
        driver.get(properties.getProperty("WH_URL"));
        driver.manage().window().maximize();
        driver.findElement(By.name("search-suggestions-nykaa")).sendKeys(properties.getProperty("search")+Keys.RETURN);
        driver.findElement(By.xpath("//*[@id=\"product-list-wrap\"]/div[1]/div/div[1]/a/div[1]/img")).click();

        Set<String> handler = driver.getWindowHandles();
        Iterator<String> it = handler.iterator();

        String HomePage=it.next(); //Parent class
        String ProductPage=it.next(); //Child class

        driver.switchTo().window(ProductPage);
        System.out.println("Product page Title:"+driver.getTitle());
        driver.close();
        driver.switchTo().window(HomePage);

        System.out.println("Home page Title:"+driver.getTitle());
        driver.close();
    }

    @AfterClass
    public void tearDown() {
        if(driver!=null)
            driver.quit();
    }
}
