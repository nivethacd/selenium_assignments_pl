import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class DynamicDropdown {
    WebDriver driver;
    private static Properties properties;

    @BeforeClass
    public void setUp(ITestContext context) {
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--disable-notifications");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        context.setAttribute("WebDriver", driver);
    }

    @Test
    public void dropdown() throws IOException {
        PropertyReader pr= new PropertyReader();
        properties = pr.propertyReader();
        driver.get(properties.getProperty("DD_URL"));
        driver.manage().window().maximize();

        String fromOptionToSelect=properties.getProperty("fromOptionToSelect");
        String toOptionToSelect=properties.getProperty("toOptionToSelect");

        //from
        driver.findElement(By.xpath("//*[@id=\"main-container\"]/div/div[1]/div[3]/div[2]/div[3]/div/div[1]/div[1]/div[2]/input")).sendKeys(properties.getProperty("fromKeyWord"));
        List<WebElement> suggestions= driver.findElements(By.xpath("//*[@id=\"main-container\"]/div/div[1]/div[3]/div[2]/div[3]/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]"));

        for(WebElement ele:suggestions){
            String currentOption=ele.getText();

            if(currentOption.contains(fromOptionToSelect))
            {
                ele.click();
                break;
            }
        }

        //to
        driver.findElement(By.xpath("//*[@id=\"main-container\"]/div/div[1]/div[3]/div[2]/div[3]/div/div[3]/div[1]/div[2]/input")).sendKeys(properties.getProperty("toKeyWord"));
        List<WebElement> suggestionsTo= driver.findElements(By.xpath("//*[@id=\"main-container\"]/div/div[1]/div[3]/div[2]/div[3]/div/div[3]/div[2]/div[2]/div[2]/div[2]"));

        for(WebElement ele:suggestionsTo){
            String currentOptionTo=ele.getText();

            if(currentOptionTo.contains(toOptionToSelect))
            {
                ele.click();
                break;
            }
        }
    }

    @AfterClass
    public void tearDown() {
        if(driver!=null)
            driver.quit();
    }
}
