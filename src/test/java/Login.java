import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;

public class Login {
    private WebDriver driver;
    private static Properties properties;

    @BeforeClass
    public void setUp(ITestContext context) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        context.setAttribute("WebDriver", driver);
    }

    @Test
    public void login() throws IOException {
        PropertyReader pr= new PropertyReader();
        properties = pr.propertyReader();
        driver.get(properties.getProperty("LoginURL"));
        driver.manage().window().maximize();
        driver.findElement(By.id("menu-item-50")).click();
        driver.findElement(By.id("username")).sendKeys(properties.getProperty("username"));
        driver.findElement(By.id("password")).sendKeys(properties.getProperty("password"));
        driver.findElement(By.name("login")).click();
        //Assert.assertTrue(driver.findElement(By.linkText("Dashboard")).isDisplayed());
        String actualUrl=properties.getProperty("actualURL");
        String expectedUrl= driver.getCurrentUrl();
        Assert.assertEquals(expectedUrl,actualUrl);
    }

    /*@Test
    public void loginFailed() throws IOException {
        PropertyReader pr= new PropertyReader();
        properties = pr.propertyReader();
        driver.get(properties.getProperty("LoginURLFail"));
        driver.manage().window().maximize();
        driver.findElement(By.linkText("Sign In")).click();
        driver.findElement(By.id("email")).sendKeys(properties.getProperty("username1"));
        driver.findElement(By.id("passwd")).sendKeys(properties.getProperty("password1"));
        driver.findElement(By.id("SubmitLogin")).click();
        Assert.assertTrue(driver.findElement(By.linkText("Dashboard")).isDisplayed());
    }*/


    @AfterClass
    public void tearDown() {
        if(driver!=null)
            driver.quit();
    }
}

